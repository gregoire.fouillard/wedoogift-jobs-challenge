# Calculator Frontend from Grégoire Fouillard

### What is this repository for? ###

This angular application aims to demonstrate the component which permits to enter an amount and gets the combination of cards corresponding.

### How do I build It? ###

Ensure you use node 16 version and install the node_modules needed for the construction of the project by running the following command.
```shell
npm install
```

And then run the following command to build the program.
```shell
npm run build
```

### How do I serve It ? ###

And then run the following command to serve the application.
```shell
npm run start
```

### More about the component

The component is named `CardFinderComponent` and is being exposed by the module `CardsFinderModule`.  
