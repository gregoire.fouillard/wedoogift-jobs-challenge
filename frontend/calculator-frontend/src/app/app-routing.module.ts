import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CardsFinderFormComponent } from './cards-finder-form/cards-finder-form.component';

const routes: Routes = [
  {
    path: '',
    component: CardsFinderFormComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
