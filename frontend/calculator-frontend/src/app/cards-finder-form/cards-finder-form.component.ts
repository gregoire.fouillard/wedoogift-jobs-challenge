import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Subscription, tap } from 'rxjs';

@Component({
  selector: 'app-cards-finder-form',
  templateUrl: './cards-finder-form.component.html',
  styleUrls: ['./cards-finder-form.component.scss']
})
export class CardsFinderFormComponent implements OnInit, OnDestroy {
  private subscription: Subscription | undefined;
  public formGroup: FormGroup;

  constructor(private formBuilder: FormBuilder) {
    this.formGroup = this.formBuilder.group({
      cardFinder: [null]
    });
  }

  ngOnInit(): void {
    this.formGroup.get('cardFinder')?.setValue({ value: 22, cards: [22] });
    this.formGroup.valueChanges.subscribe((val) => console.log(val));
  }

  ngOnDestroy(): void {
    this.subscription?.unsubscribe();
  }

  getValue(): void {
    alert(`form value ${JSON.stringify(this.formGroup.value)}`);
  }
}
