import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CardsFinderFormComponent } from './cards-finder-form.component';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { CardsFinderModule } from '../cards-finder/cards-finder.module';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';

describe('CardsFinderFormComponent', () => {
  let component: CardsFinderFormComponent;
  let fixture: ComponentFixture<CardsFinderFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CardsFinderFormComponent],
      imports: [
        ReactiveFormsModule,
        MatButtonModule,
        CardsFinderModule,
        NoopAnimationsModule
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(CardsFinderFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
