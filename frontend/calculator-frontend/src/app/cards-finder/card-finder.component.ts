import {
  Component,
  EventEmitter,
  forwardRef,
  Input,
  Output
} from '@angular/core';
import { Observable, take } from 'rxjs';
import { SearchCombinationResponse } from './models/search-combination-response';
import { HttpClient } from '@angular/common/http';
import {
  ControlValueAccessor,
  FormControl,
  NG_VALUE_ACCESSOR,
  Validators
} from '@angular/forms';
import { CalculatorComponentValue } from './models/calculator-component-value';

@Component({
  selector: 'app-card-finder',
  templateUrl: './card-finder.component.html',
  styleUrls: ['./card-finder.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => CardFinderComponent),
      multi: true
    }
  ]
})
export class CardFinderComponent implements ControlValueAccessor {
  private readonly serviceUrl = 'http://localhost:3000';
  private readonly token = 'tokenTest123';

  @Input() public shopId: number;
  @Output() amountChange = new EventEmitter<number>();

  amountControl: FormControl;
  response: SearchCombinationResponse;

  private changed = new Array<(value: CalculatorComponentValue) => void>();
  private touched = new Array<() => void>();

  constructor(private httpClient: HttpClient) {
    this.amountControl = new FormControl(0, Validators.required);
  }

  registerOnChange(fn: any): void {
    this.changed.push(fn);
  }

  registerOnTouched(fn: any): void {
    this.touched.push(fn);
  }

  setDisabledState(isDisabled: boolean): void {
    if (isDisabled) {
      this.amountControl.disable({ emitEvent: false });
    } else {
      this.amountControl.enable({ emitEvent: false });
    }
  }

  writeValue(obj: any): void {
    if (obj) {
      this.chooseAmount(obj.value, false);
    }
  }

  /**
   * Method to go to next amount which corresponds to a combination of cards
   */
  public nextAmount(): void {
    if (this.amountControl.valid) {
      const amount = this.amountControl.value + 1;
      this.searchForAmount(amount, (result) => {
        if (!result.equal && result.ceil) {
          this.chooseAmount(result.ceil.value);
        } else if (result.equal) {
          this.amountControl.setValue(amount);
          // Otherwise the response is used to display the cards corresponding
          this.response = result;
          this.emitChanges(result);
        }
      });
    }
  }

  /**
   * Method to go to previous amount which corresponds to a combination of cards
   */
  public previousAmount(): void {
    if (this.amountControl.valid) {
      const amount = this.amountControl.value - 1;
      this.searchForAmount(amount, (result) => {
        if (!result.equal && result.floor) {
          this.chooseAmount(result.floor.value);
        } else if (result.equal) {
          this.amountControl.setValue(amount);
          // Otherwise the response is used to display the corresponding cards
          this.response = result;
          this.emitChanges(result);
        }
      });
    }
  }

  /**
   * Method calling the API to get the combinations of cards corresponding of the form value 'amount'
   */
  public searchCombinations(): void {
    if (this.amountControl.valid) {
      const amount = this.amountControl.value;
      this.searchForAmount(amount, (result) => {
        if (!result.equal && !(result.ceil && result.floor)) {
          // The response is used to autocorrect input of the user
          if (result.ceil) {
            this.chooseAmount(result.ceil.value);
          } else {
            this.chooseAmount(result.floor.value);
          }
        } else {
          // Otherwise the response is used to display the corresponding cards
          this.response = result;
          this.emitChanges(result);
        }
      });
    }
  }

  private chooseAmount(amount: number, emit = true): void {
    this.searchForAmount(amount, (res) => {
      if (res.equal) {
        this.amountControl.setValue(amount);
        this.response = res;
        this.emitChanges(res);
      }
    });
  }

  private emitChanges(response: SearchCombinationResponse): void {
    this.amountChange.emit(response.equal.value);
    this.changed.forEach((fn) => fn(response.equal));
  }

  private searchForAmount(
    amount: number,
    handle: (response: SearchCombinationResponse) => void
  ): void {
    if (amount) {
      // call to backend service
      this.findCombinationOfCards(this.shopId, amount)
        .pipe(take(1))
        .subscribe({
          next: handle,
          error: (error) => {
            console.error('Error from the service backend', error);
          }
        });
    }
  }

  /**
   * Method to call for the service to get the combinations of cards for the amount requested
   * @param shopId the identifier of the shop
   * @param amount the amount requested by the user
   * @returns an observable containing the response
   */
  public findCombinationOfCards(
    shopId: number,
    amount: number
  ): Observable<SearchCombinationResponse> {
    return this.httpClient.get<SearchCombinationResponse>(
      `${this.serviceUrl}/shop/${shopId}/search-combination`,
      {
        headers: { Authorization: this.token },
        params: { amount: amount }
      }
    );
  }
}
