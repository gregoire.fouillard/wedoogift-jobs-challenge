import { SearchCombinationProposal } from './search-combination-proposal';

export class SearchCombinationResponse {
  public equal: SearchCombinationProposal;
  public floor: SearchCombinationProposal;
  public ceil: SearchCombinationProposal;
}
