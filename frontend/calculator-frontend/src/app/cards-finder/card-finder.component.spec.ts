import { ComponentFixture, inject, TestBed } from '@angular/core/testing';

import { CardFinderComponent } from './card-finder.component';
import {
  HttpClientTestingModule,
  HttpTestingController
} from '@angular/common/http/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { SearchCombinationResponse } from './models/search-combination-response';
import { MatIconModule } from '@angular/material/icon';

describe('CardFinderComponent', () => {
  let component: CardFinderComponent;
  let fixture: ComponentFixture<CardFinderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CardFinderComponent],
      imports: [
        HttpClientTestingModule,
        FormsModule,
        ReactiveFormsModule,
        MatFormFieldModule,
        MatInputModule,
        MatButtonModule,
        MatIconModule,
        NoopAnimationsModule
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(CardFinderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call backend server', inject(
    [HttpTestingController],
    (httpMock: HttpTestingController) => {
      component.findCombinationOfCards(6, 41).subscribe((response) => {
        expect(response).toBeDefined();
        expect(response.floor).toBeDefined();
        expect(response.floor.value).toBe(40);
        expect(response.floor.cards).toEqual([20, 20]);
        expect(response.ceil).toBeUndefined();
        expect(response.equal).toBeUndefined();
      });

      const req = httpMock.expectOne(
        'http://localhost:3000/shop/6/search-combination?amount=41'
      );
      req.flush(
        { floor: { value: 40, cards: [20, 20] } },
        { status: 200, statusText: 'OK' }
      );
      expect(req.request.headers.has('Authorization')).toBeTrue();
      expect(req.request.params.has('amount')).toBeTrue();
      expect(req.request.params.get('amount')).toBe('41');
      httpMock.verify();
    }
  ));
});
