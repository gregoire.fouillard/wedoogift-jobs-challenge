import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MatToolbarModule } from '@angular/material/toolbar';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CardsFinderModule } from './cards-finder/cards-finder.module';
import { CardsFinderFormComponent } from './cards-finder-form/cards-finder-form.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';

@NgModule({
  declarations: [AppComponent, CardsFinderFormComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MatToolbarModule,
    BrowserAnimationsModule,
    CardsFinderModule,
    FormsModule,
    ReactiveFormsModule,
    MatButtonModule
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
